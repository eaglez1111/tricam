import cv2
from time import time

cam_id=0
prefix="./image/img"

def make_photo():
    cap = cv2.VideoCapture(cam_id)
    k=0
    begin=time()
    while True:
        if time()-begin>=0.125*k:
            ret,frame = cap.read()
            k+=1
            if ret:
                cv2.imwrite(prefix+str(k)+".jpg", frame)
                cv2.imshow("capture", frame)
                if cv2.waitKey(1)& 0xFF == ord('q'):
                    break
            else:
                print ret, "No image"
                break
        #if time()-begin>10:
        #    break
    print "in total:",k
    print "done"
    cap.release()
    cv2.destroyAllWindows()

def takePictures4Calib():
    cap = cv2.VideoCapture(cam_id)

    cap.set(3,1920)
    cap.set(4,1080)
    cap.set(5,30)
    print "Resolution and frame rate set!"

    k=0
    while True:
        ret,frame = cap.read()

        if ret:
            cv2.imshow("capture", frame)
        else:
            print ret, "No image"
            break

        input = cv2.waitKey(1)& 0xFF

        if input == ord('c'):
            print "captured! -",k
            cv2.imwrite(prefix+str(k)+".jpg", frame)
            #cv2.imwrite("sd.jpg", frame)
            k+=1
        elif input == ord('q'):
            break

    print "in total:",k
    cap.release()
    cv2.destroyAllWindows()

def test_fps(cam_id):
    cap = cv2.VideoCapture(cam_id)

    cap.set(3,1920)
    cap.set(4,1080)
    #cap.set(5,10)
    print "Resolution and frame rate set!"

    begin=time()
    k=0
    while True:
        if time()-begin>1:
            break

        ret,frame = cap.read()
        if ret:
            print "captured! -",k
            cv2.imwrite(prefix+str(k)+".jpg", frame)
            k+=1
        else:
            print ret, "No image"
            break

        input = cv2.waitKey(1)& 0xFF
        if input == ord('q'):
            break

    print "in total:",k
    cap.release()
    cv2.destroyAllWindows()



def test3_fps():
    cap = (cv2.VideoCapture(2), cv2.VideoCapture(1), cv2.VideoCapture(0))

    for i in range(3):
        cap[i].set(3,640)
        cap[i].set(4,480)
    print "Resolution and frame rate set!"

    begin=time()
    k=0
    breakflag=0
    while True:
        if time()-begin>1:
            break

        for i in range(3):
            print i,"****"
            ret,frame = cap[i].read()
            if ret:
                print "captured! -",i," - ",k,"/n"
                cv2.imwrite(prefix+str(i)+"-"+str(k)+".jpg", frame)
            else:
                print ret, "--- No image!!!"
                breakflag=1
        k+=1

        input = cv2.waitKey(1)& 0xFF
        if input == ord('q') or breakflag==1:
            break

    print "in total:",k

    for i in range(3):
        cap[i].release()
    cv2.destroyAllWindows()




def make_video():
    cap = cv2.VideoCapture(cam_id)
    fourcc = cv2.VideoWriter_fourcc(*"DIVX")
    out = cv2.VideoWriter('xieyang.avi', fourcc, 20.0, (640,480))
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret:
            out.write(frame)
            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break
    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    print "started..."
    #make_photo()
    takePictures4Calib()
    #ctest3_fps()
    #test_fps(2)
